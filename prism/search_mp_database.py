#!/usr/bin/env python
# coding: utf-8

import argparse
import os

import pymatgen
import pymatgen.io.prismatic
import yaml
from mp_api.client import MPRester

DIRNAME = os.path.dirname(__file__)
DEFAULT_DIR = os.path.join(DIRNAME, "..", "prism_input_files")

MATERIALS_INFO_FILE = os.path.join(DIRNAME, "..", "materials_map.yml")


def get_api_key(api_file=os.path.join(DIRNAME, "materials_project_api.yaml")):
    with open(api_file, "r") as f:
        d = yaml.safe_load(f)

    api_key = d["api_key"]
    return api_key


def search_formula(formula: str | list, API_KEY: str, observed_only: bool = False):
    """

    formula: Chemical formula to search. Can include wildcards. Can be single formula
        or list of formulas
    API_KEY: API key for Materials Project
    observed_only: Only search for materials experimentally observed

    Return: list[MPDataDoc]
        list of queries with the given formula
    """

    mpr = MPRester(API_KEY)

    if isinstance(formula, str):
        formula_list = [formula]
    else:
        formula_list = formula

    # This returns a list of queries
    if observed_only:
        theoretical = not observed_only
        query_list = mpr.summary.search(formula=formula_list, theoretical=theoretical)
    else:
        query_list = mpr.summary.search(formula=formula_list)

    return query_list


def search_mp_database(formula: str | list, save_ids: bool, observed_only: bool = False):
    """
    Search materials database for given formual

    """

    # Retrieve api key
    api_key = get_api_key()

    query_list = search_formula(formula, api_key, observed_only=observed_only)

    save_str = f"Search query: {formula}\n"
    save_str += f"Number of results: {len(query_list)}\n"
    print(save_str)

    mat_ids = []

    for query in query_list:
        # Get the pretty chemical formula
        pretty_formula = query.formula_pretty  # python string

        # Get the strucutre details
        structure = query.structure  # pymatgen strucutre class

        # Symmetry information
        symmetry = query.symmetry

        # Save material information
        material_dict = {}
        mat_id = str(query.material_id)

        d = {}
        d["pretty_formula"] = pretty_formula
        d["symmetry_symbol"] = symmetry.symbol
        d["crystal_system"] = f"{symmetry.crystal_system}"
        material_dict[mat_id] = d

        # Pretty printing
        info_str = (
            f"{mat_id}\n"
            + f"  crystal_sytem: {d['crystal_system']}\n"
            + f"  pretty_formula: {pretty_formula}\n"
            + f"  symmetry_symbol: {d['symmetry_symbol'] }\n"
        )

        print(info_str)

        # For saving
        mat_ids.append(mat_id)

        existing_info: dict = {}
        if os.path.exists(MATERIALS_INFO_FILE):
            with open(MATERIALS_INFO_FILE, "r") as f:
                existing_info = yaml.safe_load(f)

        existing_info.update(material_dict)
        with open(MATERIALS_INFO_FILE, "w") as f:
            yaml.dump(existing_info, f)

    if save_ids:
        id_str = "\n".join(mat_ids)
        save_str += id_str + "\n"
        out_fname = "mp_search_results.txt"

        with open(out_fname, "w") as f:
            f.write(save_str)

    print(f"Number of results: {len(query_list)}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="search_mp_database", description="Search materials in MP database"
    )

    parser.add_argument(
        "formula",
        help="Chemical formula to search. Can include wildcards. "
        + "Multiple formulas should be separated by commas. "
        + 'Example: "Ge, *TiO3*"',
    )

    parser.add_argument(
        "--save-ids",
        default=False,
        action="store_true",
        help="Save material IDs to file 'mp_search_results.txt'",
    )

    parser.add_argument(
        "-o",
        "--observed-only",
        default=False,
        action="store_true",
        help="Only search for materials experimentally observed",
    )

    args = parser.parse_args()

    input_formula = args.formula
    save_ids = args.save_ids
    observed_only = args.observed_only

    # Convert formula to list, if needed
    formula_list = input_formula.split(",")
    formula_list = [f.strip() for f in formula_list]

    search_mp_database(formula_list, save_ids, observed_only=observed_only)
