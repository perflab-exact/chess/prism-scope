#!/usr/bin/env python
# coding: utf-8

import argparse
import os

import pandas as pd
import run_prism as rp

DIRNAME = os.path.dirname(__file__)
DEFAULT_DIR = os.path.abspath(os.path.join(DIRNAME, "..", "prism_output_files"))


def run_multiple_prism(
    input_list_file: str,
    param_file: str,
    output_dir: str = DEFAULT_DIR,
    param_combo_file: str = None,
):
    """
    Run PRISM on multiple inputs

    input_list_file: str
        Path to text file or PRISM input file to run on of all prism files to run on

    param_file: str
        Path to PRISM paramter file

    output_dir: str
        Directory to store output PRISM file

    (Optional) param_combo_file: str
        Parameter combination file to use

    """
    file_ext = os.path.splitext(input_list_file)[-1]

    flist = []
    if file_ext == ".xyz":
        flist.append(input_list_file)

    else:
        with open(input_list_file, "r") as f:
            lines = f.readlines()

        flist = [l.strip() for l in lines]

    # Retrieve combo ids
    combo_ids = None
    if param_combo_file:
        combo_df = pd.read_csv(param_combo_file, index_col="combo_id")
        combo_ids = list(combo_df.index)

    for fpath in flist:

        if combo_ids:
            for id in combo_ids:
                rp.run_prism(
                    fpath,
                    param_file,
                    output_dir=output_dir,
                    param_combo_file=param_combo_file,
                    param_combo_number=id,
                )
        else:
            rp.run_prism(fpath, param_file, output_dir=output_dir)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="run_mulitple_prism",
        description="Run multiple PRISM simulations via python",
    )

    parser.add_argument(
        "input_file_list",
        help=(
            "Path to text file or PRISM input file to run on of all prism files to"
            " run on"
        ),
    )

    parser.add_argument("param_file", help="Path to PRISM parameter file")

    parser.add_argument(
        "-d",
        "--output_dir",
        default=DEFAULT_DIR,
        help="Output directory to store PRISM *.h5 file",
    )

    parser.add_argument(
        "--param_combo_file", default=None, help="Parameter combination file to use"
    )

    args = parser.parse_args()

    input_file_list = args.input_file_list
    param_file = args.param_file
    output_dir = args.output_dir
    param_combo_file = args.param_combo_file

    run_multiple_prism(
        input_file_list,
        param_file,
        output_dir=output_dir,
        param_combo_file=param_combo_file,
    )
