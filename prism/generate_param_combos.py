#!/usr/bin/env python
# coding: utf-8

import argparse
import itertools as it
import os

import pandas as pd
import yaml

DIRNAME = os.path.dirname(__file__)
DEFAULT_COMBO_FILE = os.path.abspath(
    os.path.join(DIRNAME, "..", "prism_parameter_files", "parameter_combos.csv")
)


def generate_param_combos(save_file=None, param_combo_file=None):
    """
    Generate CSV file of combinations of desired PRSIM input parameters
    to vary when running

    save_file: CSV File to save results
    param_combo_file: File to load desired parameter variations
    """

    if not param_combo_file:
        print("Please provide an input for param_combo_file, exiting")
        return

    with open(param_combo_file, "r") as f:
        combo_dict = yaml.safe_load(f)

    column_names = sorted(combo_dict)
    combinations = it.product(*(combo_dict[Name] for Name in column_names))
    combo_list = list(combinations)
    combo_df = pd.DataFrame(combo_list, columns=column_names)
    combo_df.index.name = "combo_id"

    if save_file is None:
        if param_combo_file is None:
            save_file = DEFAULT_COMBO_FILE
        else:
            csv_name = os.path.splitext(param_combo_file)[0]
            save_file = f"{csv_name}.csv"

    print(f"Saving file: {save_file}")

    combo_df.to_csv(save_file)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="generate_param_combos",
        description="Generate necessary PRISM input files with desired rotations",
    )

    parser.add_argument(
        "-s", "--save_file", default=None, help="CSV file to save results"
    )

    parser.add_argument(
        "-f",
        "--param_combo_file",
        default=None,
        help="YAML file containing desired parameter variations",
    )

    args = parser.parse_args()

    save_file = args.save_file
    param_combo_file = args.param_combo_file

    generate_param_combos(save_file=save_file, param_combo_file=param_combo_file)
