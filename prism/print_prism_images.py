#!/usr/bin/env python
# coding: utf-8
# Print 2D images from PRISM simulation outputs

import argparse
import os
import re

import matplotlib.pyplot as plt
import numpy as np
import py4DSTEM
import yaml

DIRNAME = os.path.dirname(__file__)
DEFAULT_OUT_DIR = os.path.abspath(os.path.join(DIRNAME, "..", "prism_output_files"))
DEFAULT_IMAGE_DIR = os.path.abspath(os.path.join(DIRNAME, "..", "prism_output_images"))
MATERIALS_INFO_FILE = os.path.abspath(os.path.join(DIRNAME, "..", "materials_map.yml"))

# mp-5229_1_0_0_angle_0.h5
OLD_PATTERN = re.compile("mp-(\d+)_(\d)_(\d)_(\d)_angle_(\d+).h5$")

# mp-5229_1_0_0_angle_0_10nm.h5
OLD_PATTERN_Z = re.compile("mp-(\d+)_(\d)_(\d)_(\d)_angle_(\d+)_(\d+)nm.h5$")

# mp-5229_1_0_0_angle_0_combo_11.h5
OLD_COMBO_PATTERN = re.compile("mp-(\d+)_(\d)_(\d)_(\d)_angle_(\d+)_combo_(\d+).h5$")

# mp-5229_1_0_0_angle_0_10nm_combo_11.h5
OLD_COMBO_PATTERN_Z = re.compile(
    "mp-(\d+)_(\d)_(\d)_(\d)_angle_(\d+)_(\d+)nm_combo_(\d+).h5$"
)

# #mp-5229_110_10nm.h5
HJK_PATTERN = re.compile("mp-(\d+)_(\d)(\d)(\d)_(\d+)nm.h5$")

# #mp-5229_110_10nm_combo_39.h5
HJK_COMBO_PATTERN = re.compile("mp-(\d+)_(\d)(\d)(\d)_(\d+)nm_combo_(\d+).h5$")


def _get_formula(material_id: str):
    """
    Retrieve material formual
    """

    with open(MATERIALS_INFO_FILE, "r") as f:
        mat_map = yaml.safe_load(f)
    formula = mat_map[material_id]["pretty_formula"]

    return formula


def _retrieve_plt_title(fname: str):
    """
    Retrieve necessary info to add plot title
    """

    plt_title = ""

    match = OLD_PATTERN.match(fname)
    if match:
        groups = match.groups()
        mat_id = f"mp-{groups[0]}"
        rotation_axis = groups[1:4]
        rotation_string = f"[{rotation_axis[0]}{rotation_axis[1]}{rotation_axis[2]}]"
        angle = groups[4]

        formula = _get_formula(mat_id)

        plt_title = f"{formula} {rotation_string} {angle} deg"

    match = OLD_PATTERN_Z.match(fname)
    if match:
        groups = match.groups()
        mat_id = f"mp-{groups[0]}"
        rotation_axis = groups[1:4]
        rotation_string = f"[{rotation_axis[0]}{rotation_axis[1]}{rotation_axis[2]}]"
        angle = groups[4]
        height = groups[5]

        formula = _get_formula(mat_id)

        plt_title = f"{formula} {rotation_string} {angle} deg {height}nm"

    match = OLD_COMBO_PATTERN.match(fname)
    if match:
        groups = match.groups()
        mat_id = f"mp-{groups[0]}"
        rotation_axis = groups[1:4]
        rotation_string = f"[{rotation_axis[0]}{rotation_axis[1]}{rotation_axis[2]}]"
        angle = groups[4]
        combo = groups[5]

        formula = _get_formula(mat_id)
        plt_title = f"{formula} {rotation_string} {angle} deg - combo {combo}"

    match = OLD_COMBO_PATTERN_Z.match(fname)
    if match:
        groups = match.groups()
        mat_id = f"mp-{groups[0]}"
        rotation_axis = groups[1:4]
        rotation_string = f"[{rotation_axis[0]}{rotation_axis[1]}{rotation_axis[2]}]"
        angle = groups[4]
        height = groups[5]
        combo = groups[6]

        formula = _get_formula(mat_id)
        plt_title = f"{formula} {rotation_string} {angle} deg {height}nm- combo {combo}"

    match = HJK_PATTERN.match(fname)
    if match:
        groups = match.groups()
        mat_id = f"mp-{groups[0]}"
        rotation_axis = groups[1:4]
        rotation_string = f"[{rotation_axis[0]}{rotation_axis[1]}{rotation_axis[2]}]"
        height = groups[4]

        formula = _get_formula(mat_id)
        plt_title = f"{formula} {rotation_string} {height}nm"

    match = HJK_COMBO_PATTERN.match(fname)
    if match:
        groups = match.groups()
        mat_id = f"mp-{groups[0]}"
        rotation_axis = groups[1:4]
        rotation_string = f"[{rotation_axis[0]}{rotation_axis[1]}{rotation_axis[2]}]"
        height = groups[4]
        combo = groups[5]

        formula = _get_formula(mat_id)
        plt_title = f"{formula} {rotation_string} {height}nm - combo {combo}"

    return plt_title


def print_prism_images(
    output_file=None,
    output_dir=DEFAULT_OUT_DIR,
    image_dir=DEFAULT_IMAGE_DIR,
):
    os.makedirs(image_dir, exist_ok=True)

    print(output_file)

    bname = os.path.basename(output_file)

    plt_title = _retrieve_plt_title(bname)

    plt_bname = f"{bname.split('.')[0]}"

    # Read in the file. This will show the data shape of the different structures. Descriptions fo the various outputs can be found here: https://prism-em.com/docs-outputs/
    # The `virtual_detector` is a 3D array that represents a radially integrated output. Like above, the first 2 dimensions represent the probe position. The last dimension represents the detection angles of the of the probe

    virtual_detector = py4DSTEM.io.read(
        output_file, data_id="virtual_detector_depth0000"
    )

    # We visaulize a slice of the 3D array
    # We can also visualiz sum of integrated angles, simulating the sliders selecting virtual inner and outer angles in the PRSIM gui

    # Here, we generate 4 different looks from different detection angles
    # - virtual bright field (BF)
    # - annular bright field (ABF)
    # - annular dark field (ADF)
    # - high angle annular dark field (HAADF)

    cmap = "gray"

    max_angle = virtual_detector.data.shape[-1]

    inner_angles = [0, 11, 20, 61]
    outer_angles = [20, 20, 40, max_angle]
    labels = ["BF", "ABF", "ADF", "HAADF"]

    data_list = []

    for inner, outer in zip(inner_angles, outer_angles):
        im = virtual_detector.data[:, :, inner:outer]
        data_list.append(np.sum(im, axis=-1))

    fig, _ = plt.subplots(2, 2, figsize=(10, 10))

    for i in range(4):
        ax = plt.subplot(2, 2, i + 1)

        inner = inner_angles[i]
        outer = outer_angles[i]

        title = f"{labels[i]}: {inner}-{outer} millirads"

        data_array = data_list[i]
        data_array = data_array.transpose()

        plt.imshow(
            data_array, cmap=cmap, interpolation=None, resample=False, origin="lower"
        )
        ax.set_title(title)

    fig.suptitle(plt_title)
    fig.tight_layout()
    fig.subplots_adjust(top=0.88)

    fpath = os.path.join(image_dir, f"{plt_bname}_summary.png")
    print(fpath)
    plt.savefig(fpath, bbox_inches="tight")
    plt.close()

    # Save each view images as a`.tiff` file
    for i in range(4):
        data_array = data_list[i]
        data_array = data_array.transpose()

        fig = plt.imshow(
            data_array, cmap="gray", interpolation=None, resample=False, origin="lower"
        )

        plt.axis("off")

        fpath = os.path.join(image_dir, f"{plt_bname}_{labels[i]}.tiff")
        print(fpath)
        plt.savefig(fpath, bbox_inches="tight", pad_inches=0)
        plt.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="print_prism_outputs",
        description="Print 2D images from PRISM simulation output",
    )

    parser.add_argument(
        "-p", "--output_file_path", default=None, help="Path to PRISM output h5 file"
    )

    parser.add_argument(
        "-o",
        "--output_dir",
        default=DEFAULT_OUT_DIR,
        help=(
            "Directory h5 output file is located, if not using path. "
            f"Default is {DEFAULT_OUT_DIR}"
        ),
    )

    parser.add_argument(
        "-d",
        "--image_dir",
        default=DEFAULT_IMAGE_DIR,
        help=(
            "Directory to stored generated prism output file. "
            f"Default is {DEFAULT_IMAGE_DIR}"
        ),
    )

    args = parser.parse_args()

    output_file_path = args.output_file_path
    output_dir = args.output_dir
    image_dir = args.image_dir

    print_prism_images(
        output_file=output_file_path,
        output_dir=output_dir,
        image_dir=image_dir,
    )
