#!/usr/bin/env python
# coding: utf-8
# Print 2D images from PRISM simulation outputs

import os
import argparse
import glob

import print_prism_images

def print_multi_prism_images(output_dir: str, image_dir: str):

    os.makedirs(image_dir, exist_ok=True)
    h5_files = glob.glob(f"{output_dir}/*h5")

    for h5_file in h5_files:
        print(f"Generating images for: {h5_file}")
        print_prism_images.print_prism_images(output_file=h5_file, image_dir=image_dir)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(prog = 'print_multi_prism_images', 
                description = 'Print 2D images from PRISM simulation outputs in a given directory')

    parser.add_argument('-o', '--output_dir',
                help = f'Directory the PRISM h5 output files are located')
    
    parser.add_argument('-d', '--image_dir', 
                help = f'Directory to stored generated prism images.')



    args = parser.parse_args()
    output_dir = args.output_dir
    image_dir = args.image_dir

    print_multi_prism_images(output_dir=output_dir, image_dir=image_dir)