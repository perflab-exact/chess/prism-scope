#!/usr/bin/env python
# coding: utf-8

import argparse
import os
import socket

import pandas as pd
import pyprismatic as pr
import yaml
from calc_material_thickness import read_material_height

DIRNAME = os.path.dirname(__file__)
DEFAULT_DIR = os.path.join(DIRNAME, "..", "prism_output_files")
MATERIALS_MAP = os.path.join(DIRNAME, "..", "materials_map.yml")


def set_meta_field(field: str, value, meta: pr.Metadata):
    """
    Set the meta field with the desired value
    """

    # This is incorrectly cast as float
    # meta.earlyCPUStopCount = int(meta.earlyCPUStopCount)
    missed_int_fields = ["randomSeed", "earlyCPUStopCount"]

    if field in meta.str_fields:
        setattr(meta, field, value)

    elif field in meta.int_fields or field in missed_int_fields:
        setattr(meta, field, int(value))

    elif field in meta.float_fields:
        setattr(meta, field, float(value))

    else:
        if isinstance(value, str):
            bool_value = value == "True"
        else:
            bool_value = value

        setattr(meta, field, bool_value)


def read_prism_parameters(
    param_file: str, meta: pr.Metadata, param_combo_file: str = None, param_combo_number: int = None
):
    """
    Read parameters from PRISM parameter file and adjust attributes
    to correctly work with pyprsimatic

    param_file: Parameter file to read from
    meta: pr.Metadata object for PRISM

    param_combo_file: Paramter combination CSV file to add varying paramters
    param_combo_number: Combination number to add

    """

    with open(param_file, "r") as f:
        for line in f:
            line = line.strip()
            field, value = line.split(" = ")
            set_meta_field(field, value, meta)

    if param_combo_file:
        param_combo_df = pd.read_csv(param_combo_file, index_col="combo_id")

        if param_combo_number not in list(param_combo_df.index):
            print(
                f"Combination number {param_combo_number} does not exist "
                + f"in parameter combination file {param_combo_file}"
            )
            return 0

        combo = param_combo_df.loc[param_combo_number]

        param_names = list(combo.index)

        print(param_names)

        for field in param_names:
            value = combo[field]
            print(field, value)
            set_meta_field(field, value, meta)

    return 1


def run_prism(
    fpath: str,
    param_file: str,
    output_dir: str = DEFAULT_DIR,
    output_name: str = None,
    param_combo_file: str = None,
    param_combo_number: int = None,
    info_file: str = None,
):
    """
    Run PRISM

    fpath: str
        Path to PRISM input file (*.xyz)

    param_file: str
        Path to PRSIM paramter file

    output_dir: str
        Directory to store output PRISM file

    (Optional) output_name :str
        Name of output .h5 file. Defualt is Same name as PRSIM input file with the
        *.xyz extension replaced with *.h5

    (Optional) param_combo_file: str
        Parameter combination file to use

    (Optional) param_combo_number: int
        Parameter combination number

    (Optional) info_file: str
        Whether to log relevant information for the output file
    """

    if param_combo_file is None != param_combo_number is None:
        print("You must supply both a param_combo_file and param_combo_number")
        return

    fpath = os.path.abspath(fpath)
    bname = os.path.basename(fpath)

    os.makedirs(output_dir, exist_ok=True)

    meta = pr.Metadata(filenameAtoms=fpath)

    status = read_prism_parameters(
        param_file, meta, param_combo_file=param_combo_file, param_combo_number=param_combo_number
    )

    if status == 0:
        return

    # Determine thickness
    coords = read_material_height(fpath)
    unit_height = coords[-1]

    thickness = meta.tileZ * unit_height / 10.0
    thickness = round(thickness)

    if param_combo_number is not None:
        output_bname = f"{bname[:-4]}_{thickness}nm_combo_{param_combo_number}.h5"
    else:
        output_bname = f"{bname[:-4]}_{thickness}nm.h5"

    output_name = os.path.join(output_dir, output_bname)

    meta.filenameOutput = output_name

    meta.go(save_run_time=True)

    if info_file:
        # Add to text log of files created, if requested
        interpX = meta.interpolationFactorX
        interpY = meta.interpolationFactorY

        # system run on
        hostname = socket.gethostname()

        # Add chemical formula name
        mat_id = bname.split("_")[0]

        with open(MATERIALS_MAP, "r") as f:
            mat_map = yaml.safe_load(f)

        formula = mat_map[mat_id]["pretty_formula"]

        # Whether use GPU (needs to be post-processing)
        log_info = {
            "h5_file": output_name,
            "input_file": fpath,
            "param_file": param_file,
            "combo_file": param_combo_file,
            "combo_number": param_combo_number,
            "interpX": interpX,
            "interpY": interpY,
            "mp_id": mat_id,
            "formula": formula,
            "hostname": hostname,
            "GPU": "unknown",
        }

        header_list = [key for key in log_info]
        value_list = [f"{log_info[key]}" for key in log_info]

        header_str = ",".join(header_list) + "\n"
        value_str = ",".join(value_list) + "\n"

        if os.path.exists(info_file):
            with open(info_file, "a") as f:
                f.write(value_str)
        else:
            with open(info_file, "w") as f:
                f.write(header_str)
                f.write(value_str)

        print(f"Writing to log: {info_file}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog="run_prism", description="Run PRISM software via python")

    parser.add_argument("fpath", help="Path to PRISM input file")

    parser.add_argument("param_file", help="Path to PRISM parameter file")

    parser.add_argument(
        "-d", "--output_dir", default=DEFAULT_DIR, help="Output directory to store PRISM *.h5 file"
    )

    parser.add_argument(
        "-n",
        "--output_name",
        default=None,
        help=" Name of output .h5 file. Defualt is Same name as PRISM input file with the \
                        *.xyz extension replaced with *.h5",
    )

    parser.add_argument(
        "--param_combo_file", default=None, help="Parameter combination file to use"
    )

    parser.add_argument(
        "--param_combo_number",
        default=None,
        help="Parameter combination number from param_combo_file",
    )

    parser.add_argument("--log_file", default=None, help="File to log additional information")

    args = parser.parse_args()

    fpath = args.fpath
    param_file = args.param_file
    output_dir = args.output_dir
    output_name = args.output_name
    param_combo_file = args.param_combo_file
    param_combo_number = args.param_combo_number
    log_file = args.log_file

    if param_combo_number:
        param_combo_number = int(param_combo_number)

    run_prism(
        fpath,
        param_file,
        output_dir=output_dir,
        output_name=output_name,
        param_combo_file=param_combo_file,
        param_combo_number=param_combo_number,
        info_file=log_file,
    )
