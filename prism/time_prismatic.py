#!/usr/bin/env python
# coding: utf-8

# time running pristmatic with different settings

import os
import time
import datetime
import run_prism as rp
import pandas as pd

fpath = "../prism_input_files/mp-5229_1_0_0_angle_0.xyz"
param_file="../prism_parameter_files/base_params.txt"
output_dir="../timing_outputs"
param_combo_file="../prism_parameter_files/timing_numFP_param.csv"

# Retrieve combo ids
combo_df = pd.read_csv(param_combo_file, index_col='combo_id')

param_names = list(combo_df.columns)

# Retrieve param names that will be varied
param_header = ""
for pn in param_names:
    param_header = f"{param_header}{pn},"

# Retrice column ids
combo_ids = list(combo_df.index)

header = f"param_file, {param_header} elapsed time (s), readable time"
time_file = "prism_numFP_times.csv"

if os.path.exists(time_file):
    print(f"File time_file already exists: specifiy a differtn save file")
    exit(0)

with open(time_file, 'w') as f:
    f.write(f"{header}\n")

for combo_id in combo_ids:

    start_time = time.time()
    rp.run_prism(fpath, param_file, output_dir=output_dir,
                 param_combo_file=param_combo_file, param_combo_number=int(combo_id))

    end_time = time.time()

    total_time = end_time - start_time

    # hh:mm:ss fomrat
    str_time = str(datetime.timedelta(seconds=total_time))
                
    # Save results
    combo = combo_df.loc[combo_id]
    combo_str = ""
    for v in combo.values:
        combo_str = f"{combo_str}{v},"

    strline = f"{param_file},{combo_str}{total_time},{str_time}"

    with open(time_file, 'a') as f:
        f.write(f"{strline}\n")
