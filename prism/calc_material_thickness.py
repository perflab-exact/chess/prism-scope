#!/usr/bin/env python
# coding: utf-8

import argparse
import itertools
import os

import pandas as pd

DIRNAME = os.path.dirname(__file__)
DEFAULT_CSV_DIR = os.path.abspath(os.path.join(DIRNAME, "..", "prism_parameter_files"))

# default thickness in nm
DEFAULT_THICKNESS = [10, 25, 50, 100]

# default width in nm
# (tileY)
DEFAULT_WIDTH = [4]

# (tileX) default length in nm
DEFAULT_LENGTH = [4]


def read_material_height(prism_input_file: str):
    """
    Retrieve unit coordinates of material from .xyz PRISM file

    Return:
        list of coordiates (length, width, height) in Angstroms
    """
    flines = []
    with open(prism_input_file, "r") as f:
        flines = f.readlines()

    coord_line = flines[1].strip()

    coords = [float(c) for c in coord_line.split(" ")]

    return coords


def calc_material_thickness(
    prism_input_file: str,
    desired_length=DEFAULT_LENGTH,
    desired_width=DEFAULT_WIDTH,
    desired_thickness=DEFAULT_THICKNESS,
):
    """
    Calculate desired tileZ parameter sizes to achieved desired
    material thickness for PRISM simulations

    xyz_file: PRISM .xyz input file
    desired_thickness: List of desired material thickness, in nm

    Returns:
        List of tileZ parameters, round to nearest whole number
    """

    coords = read_material_height(prism_input_file)
    # in Angstrom
    length = coords[0]
    width = coords[1]
    height = coords[2]

    # h * tileZ / 10 = nm
    # 10*nm/h = tileZ
    tileZs = [round(10 * n / height) for n in desired_thickness]

    tileXs = [round(10 * n / length) for n in desired_length]
    tileYs = [round(10 * n / width) for n in desired_width]

    return tileXs, tileYs, tileZs


def _calc_num_fp(df: pd.DataFrame, coords):
    """
    Determine number of frozen phonons to use

    Return DataFraemw ith numFP added
    """
    num_fps = []

    height = coords[2]

    for i, row in df.iterrows():
        tileZ = row["tileZ"]

        # thickness = height * tileZ / 10

        num_fp = 16
        # Increase to 16 for thicker material
        # if thickness >= 40:
        # num_fp = 16

        num_fps.append(num_fp)

    df["numFP"] = num_fps

    return df


def create_combo_file(prism_input_file, tileXs, tileYs, tileZs, csv_dir):
    # name of combo file is basename.csv
    bname = os.path.basename(prism_input_file)

    mp_id = bname.split("_")[0]

    fprefix = os.path.splitext(bname)[0]
    csv_name = f"{fprefix}_tile_params.csv"

    # Iterate on the combinations
    a = [tileXs, tileYs, tileZs]

    combos = list(itertools.product(*a))

    col_names = ["tileX", "tileY", "tileZ"]

    df = pd.DataFrame(combos, columns=col_names)
    df.index.name = "combo_id"

    coords = read_material_height(prism_input_file)
    df = _calc_num_fp(df, coords)

    save_dir = os.path.join(csv_dir, mp_id)
    os.makedirs(save_dir, exist_ok=True)

    csv_path = os.path.join(save_dir, csv_name)

    df.to_csv(csv_path)

    return csv_path


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="calc_material_thickness",
        description="Generate parameter combo file for desired material thickness",
    )

    parser.add_argument("prism_input_file", help="Path to PRISM input file")

    parser.add_argument(
        "-d",
        "--csv_dir",
        default=DEFAULT_CSV_DIR,
        help="Directory to store csv param file",
    )

    args = parser.parse_args()

    prism_input_file = args.prism_input_file
    csv_dir = args.csv_dir

    tileXs, tileYs, tileZs = calc_material_thickness(prism_input_file)
    csv_path = create_combo_file(prism_input_file, tileXs, tileYs, tileZs, csv_dir)

    print(f"Create combo file: {csv_path}")
