import os

import abtem
import ase
import ase.io
import numpy as np
import py4DSTEM
import pymatgen.core.structure
from pymatgen.io.ase import AseAtomsAdaptor


def rotate_cubic_crystal(prism_file: str):
    """Rotate and save rotations for a cubic crystal.

    Rotations are saved in the same directory as prism_file input. Rotatiosn
    that are saved are: [MP_ID]_100.xyz, [MP_ID]_110.xyz, and [MP_ID]_111.xyz,
    where [MPD_ID] is the material ID of the crystal. Files are saved in
      the same directory as `prism_file`

    Args:
        prism_file (str): Path to pristmatic file for cubic material. This is the
        file generated from the `mp_to_prismatic.py` script
        Assumes prism_file has structure [MP_ID].xyz, where [MPD_ID] is the
        material ID of the crystal


    Returns
    ----------
        list[str]: List of rotated prismatic files

    """
    rotated_files = []

    save_dir = os.path.dirname(prism_file)
    bname = os.path.basename(prism_file)
    fprefix = os.path.splitext(bname)[0]
    mp_id = fprefix

    structure = ase.io.read(prism_file, format="prismatic")

    #######################################
    # Save structure as [100] orientation
    #######################################
    new_fname = os.path.join(save_dir, f"{mp_id}_100.xyz")
    rotated_files.append(new_fname)
    ase.io.write(filename=new_fname, images=structure, format="prismatic")
    print(f"Saved file: {new_fname}")

    #######################################
    # Rotate structure to [110] orientation
    #######################################
    zone_axis_cartesian = (1, 0, 1)

    structure_rotate = structure.copy()
    structure_rotate.rotate(
        a=zone_axis_cartesian,
        v=(0, 0, 1),
        rotate_cell=True,
    )

    # Orthogonalize and tile unit cell
    structure_orthogonal = abtem.orthogonalize_cell(structure_rotate)
    abtem.show_atoms(structure_orthogonal, plane="xy")

    # Save [110] structure
    new_fname = os.path.join(save_dir, f"{mp_id}_110.xyz")
    rotated_files.append(new_fname)
    ase.io.write(filename=new_fname, images=structure_orthogonal, format="prismatic")

    print(f"Saved file: {new_fname}")

    #######################################
    # Rotate structure to [111] orientation
    #######################################

    structure_rotate = structure.copy()
    structure_rotate.rotate(
        a=(1, 0, 1),
        v=(0, 0, 1),
        rotate_cell=True,
    )

    structure_rotate.rotate(
        a=35,
        v=(1, 0, 0),
        rotate_cell=True,
    )

    # Orthogonalize and tile unit cell
    structure_orthogonal = abtem.orthogonalize_cell(structure_rotate)

    # Save [111] strucutre
    new_fname = os.path.join(save_dir, f"{mp_id}_111.xyz")
    rotated_files.append(new_fname)
    ase.io.write(filename=new_fname, images=structure_orthogonal, format="prismatic")

    print(f"Saved file: {new_fname}")

    return rotated_files


def create_zone_axes(
    struct: pymatgen.core.structure.Structure,
    num_zones: int = 10,
    angle_step_zone_axis: float = 0.5,
    k_max: float = 1,
    max_hkl: int = 5,
    seed: int = 42,
    tol_den: int = 4,
    random_zones: bool = False,
) -> np.ndarray:
    """Create non-symmetrical equivalent HKL vectors

    Args:
        struct (pymatgen.core.structure.Structure): Pymatgen structure
        num_zones (int, optional): Number of zones to return, if bigger than total provided it will give all zones.
            If negative, return all zones. Defaults to 10.
        angle_step_zone_axis (float, optional): step size, smaler step gives more zones,
            but will be high index. Defaults to 0.5.
        k_max (float, optional):  I don't think this matters for orientation plan.
            Defaults to 1.
        max_hkl (int, optional): largest vectors to include inclusive. Defaults to 5.
        seed (int, optional): random seed set for reproducibility. Defaults to 42.
        tol_den (int, optional): function to rationalise the indicies. Defaults to 4.
        random_zones (bool, optional): Randomize zones. Defaults to False.

    Returns:
        np.ndarray: Non-symmetrical HKL vectors
    """
    # create a py4DSTEM crystal
    crystal = py4DSTEM.process.diffraction.Crystal.from_pymatgen_structure(struct)

    # calculate the structure factors
    crystal.calculate_structure_factors(k_max)

    # calculate the orientation plan
    crystal.orientation_plan(
        angle_step_zone_axis=angle_step_zone_axis,
        zone_axis_range="auto",
        # calculate_correlation_array = False,
    )

    #  self.orientation_ref
    zones = np.unique(
        np.apply_along_axis(
            crystal.rational_ind,
            axis=1,
            arr=crystal.orientation_vecs,
            tol_den=tol_den,
        ),
        axis=0,
    )
    # filter k to keep the low index
    filtered_zones = zones[(abs(zones) <= max_hkl).all(axis=1)]

    print(f"Number of zones: {len(filtered_zones)}")

    if num_zones < 0:
        num_zones = len(filtered_zones)

    # if not random pick the lowest ones
    if not random_zones:
        if filtered_zones.shape[0] > num_zones:
            return filtered_zones[:num_zones]

        else:
            return filtered_zones
    # rng
    rng = np.random.default_rng(seed=seed)

    # check if the number of filtered zones is longer than the number requested
    if filtered_zones.shape[0] > num_zones:
        # pick random zones from the filtered list if there are more zones than
        zones = rng.permutation(filtered_zones)[:num_zones]

        return zones

    # if its not longer
    else:
        print(
            f"less zones ({filtered_zones.shape[0]}) returned than requested ({num_zones}), try increasing max_hkl to ensure sufficent zones"
        )
        # shuffle the array and return it
        return filtered_zones


def rotate_and_tile_atoms(
    struct: pymatgen.core.structure.Structure,
    proj_dir=(0, 0, 1),
    proj_dir_cartesian=None,
    cell_size=(50, 50, 50),
    return_cell_params: bool = False,
) -> ase.Atoms | tuple[ase.Atom, np.ndarray, np.ndarray]:
    """
    Rotate and tile unit cell to fill a larger orthogonal cell. Imports N sites, tiles to M sites.

    Parameters
    ----------
    struct: pymatgen.Structure
        pymatgen structure to be roated, tiled and returned as an ase object
    proj_dir: np.array
        (3, ) projection direction in terms of the u,v,w vectors.
    proj_dir_cartesian: np.array
        (3, ) projection direction in terms of (x,y,z) coordinates.
    cell_size:
        (3, ) cell size (Angstroms).

    Returns
    ----------
    structure: ase.Atoms
        ase.atoms structure
    xyz_tile, num_tile: (3,)
        optional returns

    """

    atoms = AseAtomsAdaptor.get_atoms(struct)
    uvw = np.array(atoms.cell.data)
    pos = atoms.get_scaled_positions()
    num = atoms.numbers
    # projection vectors
    if proj_dir_cartesian is None:
        #         w_proj = uvw @ np.array(proj_dir).astype('float')
        #         w_proj = uvw @ np.array(proj_dir).astype('float')
        w_proj = (
            uvw[0, :] * proj_dir[0] + uvw[1, :] * proj_dir[1] + uvw[2, :] * proj_dir[2]
        )
    else:
        w_proj = np.array(proj_dir_cartesian).astype("float")
    w_proj /= np.linalg.norm(w_proj)
    if w_proj[0] < 1e-3:
        u_proj = np.array((1.0, 0, 0))
    else:
        u_proj = np.array((0, 1.0, 0))
    v_proj = np.cross(w_proj, u_proj)
    v_proj /= np.linalg.norm(v_proj)
    u_proj = np.cross(v_proj, w_proj)

    proj = np.linalg.inv(
        np.vstack(
            (
                u_proj,
                v_proj,
                w_proj,
            )
        )
    )
    uvw_proj = uvw @ proj

    # Determine tiling range
    pos_corner = np.array(
        (
            (0, 0, 0),
            (cell_size[0], 0, 0),
            (0, cell_size[1], 0),
            (cell_size[0], cell_size[1], 0),
            (0, 0, cell_size[2]),
            (cell_size[0], 0, cell_size[2]),
            (0, cell_size[1], cell_size[2]),
            (cell_size[0], cell_size[1], cell_size[2]),
        )
    )
    abc = pos_corner @ np.linalg.inv(uvw_proj)
    # print(abc.round(2))
    a_range = (
        np.floor(np.min(abc[:, 0])).astype("int"),
        np.ceil(np.max(abc[:, 0])).astype("int"),
    )
    b_range = (
        np.floor(np.min(abc[:, 1])).astype("int"),
        np.ceil(np.max(abc[:, 1])).astype("int"),
    )
    c_range = (
        np.floor(np.min(abc[:, 2])).astype("int"),
        np.ceil(np.max(abc[:, 2])).astype("int"),
    )

    # Tiling indices
    a, b, c, ind = np.meshgrid(
        np.arange(a_range[0], a_range[1]),
        np.arange(b_range[0], b_range[1]),
        np.arange(c_range[0], c_range[1]),
        np.arange(pos.shape[0]),
        indexing="ij",
    )
    abc_ind_tile = np.vstack(
        (
            a.ravel(),
            b.ravel(),
            c.ravel(),
            ind.ravel(),
        )
    )

    # Cartesian coordinates
    abc_tile = abc_ind_tile[:3] + pos[abc_ind_tile[3, :], :].T
    xyz_tile = (
        abc_tile[0][:, None] * uvw_proj[0, :]
        + abc_tile[1][:, None] * uvw_proj[1, :]
        + abc_tile[2][:, None] * uvw_proj[2, :]
    )

    # Atomic identities
    num_tile = num[abc_ind_tile[3, :]]

    # delete atoms outsize of cell boundaries
    keep = np.logical_and.reduce(
        (
            xyz_tile[:, 0] >= 0.0,
            xyz_tile[:, 1] >= 0.0,
            xyz_tile[:, 2] >= 0.0,
            xyz_tile[:, 0] < cell_size[0],
            xyz_tile[:, 1] < cell_size[1],
            xyz_tile[:, 2] < cell_size[2],
        )
    )

    xyz_tile = xyz_tile[keep, :]
    num_tile = num_tile[keep]

    # Convert to an ASE structure
    structure = ase.Atoms(
        positions=xyz_tile,
        numbers=num_tile,
        cell=cell_size,
    )

    # Set Debye-Waller Factors to 0
    natoms = num_tile.shape[0]
    debye_waller_factos = np.zeros(natoms)
    structure.set_array("debye_waller_factors", debye_waller_factos)

    if not return_cell_params:
        return structure
    else:
        return structure, xyz_tile, num_tile


def rotate_crystal(
    prism_file: str,
    num_zones: int = -1,
    angle_step_zone_axis: float = 0.5,
    k_max: float = 1,
    max_hkl: int = 2,
    cell_size=[10, 10, 10],
) -> list[str]:
    """Identify and rotate crystals along symmetry axes

    Args:
        prism_file (str): Path to prism file input. Assumes prism_file has structure [MP_ID].xyz, where [MPD_ID] is the
        material ID of the crystal
        num_zones (int, optional): Number of symmetry zones to roate by if bigger than total provided it will give all zones.
            If negative, retrun all zones. Defaults to -1.
        angle_step_zone_axis (float, optional): Steps rotate by when findinx zones. Defaults to 0.5.
        k_max (float, optional): _description_. Defaults to 1.
        max_hkl (int, optional): Largest zone vectors to include (inclusive). Defaults to 2.
        cell_size (list, optional): Size of cell (in angstroms) to save rotated material.
            Defaults to [20, 20, 20].

    Returns
    --------

    """
    rotated_files = []

    save_dir = os.path.dirname(prism_file)
    bname = os.path.basename(prism_file)
    fprefix = os.path.splitext(bname)[0]
    mp_id = fprefix

    ase_structure = ase.io.read(prism_file, format="prismatic")

    pymtagen_structure = AseAtomsAdaptor.get_structure(ase_structure)

    # Find zones fo symmetry
    zones = create_zone_axes(
        pymtagen_structure,
        angle_step_zone_axis=angle_step_zone_axis,
        k_max=k_max,
        max_hkl=max_hkl,
        num_zones=num_zones,
    )

    # save file for each symmetry zone
    for zone in zones:
        rotate_ase_struct = rotate_and_tile_atoms(
            pymtagen_structure, proj_dir=zone, cell_size=cell_size
        )

        zone_str = "".join([f"{x}" for x in zone])
        new_fname = os.path.join(save_dir, f"{mp_id}_{zone_str}.xyz")
        rotated_files.append(new_fname)
        print(f"Saving file: {new_fname}")
        ase.io.write(filename=new_fname, images=rotate_ase_struct, format="prismatic")

    return rotated_files
