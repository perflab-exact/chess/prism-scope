#!/usr/bin/env python
# coding: utf-8

import argparse
import os

import abtem
import ase
import crystal_rotation
import matplotlib.pyplot as plt
import pymatgen
import pymatgen.io.prismatic
import yaml
from mp_api.client import MPRester

DIRNAME = os.path.dirname(__file__)
DEFAULT_DIR = os.path.join(DIRNAME, "..", "prism_input_files")

MATERIALS_INFO_FILE = os.path.join(DIRNAME, "..", "materials_map.yml")


def get_api_key(api_file=os.path.join(DIRNAME, "materials_project_api.yaml")):
    with open(api_file, "r") as f:
        d = yaml.safe_load(f)

    api_key = d["api_key"]
    return api_key


def retrieve_material_id(mp_id: str, API_KEY: str):
    """
    Retrieve given material from Material Project.

    mp_id: Materials Project material ID
    API_KEY: API key for Materials Project

    Return: MPDataDoc struct

    """
    # mpr = MPRester(API_KEY)

    # This returns a list of dictionaries, our query has one entry
    query_mpr = MPRester(API_KEY, use_document_model=False)
    query_list = query_mpr.materials.summary.search(material_ids=[mp_id])
    query = query_list[0]

    struct_mpr = MPRester(API_KEY)
    structure = struct_mpr.get_structure_by_material_id(
        mp_id, conventional_unit_cell=True
    )

    return query, structure


def mp_to_prismatic(
    mp_id,
    prism_file_dir: str = DEFAULT_DIR,
    rotate: bool = False,
    preview_cells: bool = False,
):
    """
    Take Material ID, apply rotations, and save as PRISM input file (*.xyz)

    mp_id: Materials Project material ID
    prism_file_dir: Directory to store prism innput file
    rotate: Appply appropriate rotations materials
    preview_cells: Print preview of cells

    """

    # Retrieve api key
    api_key = get_api_key()

    query, structure = retrieve_material_id(mp_id, api_key)

    # Get the pretty chemical formula
    pretty_formula = query["formula_pretty"]  # python string

    # Symmetry information
    symmetry = query["symmetry"]
    crystal_system = symmetry["crystal_system"]

    # Save material information
    material_dict = {}
    d = {}
    d["pretty_formula"] = pretty_formula
    d["symmetry_symbol"] = symmetry["symbol"]
    d["crystal_system"] = f"{crystal_system}"
    material_dict[mp_id] = d

    existing_info: dict = {}
    if os.path.exists(MATERIALS_INFO_FILE):
        with open(MATERIALS_INFO_FILE, "r") as f:
            existing_info = yaml.safe_load(f)

    existing_info.update(material_dict)
    with open(MATERIALS_INFO_FILE, "w") as f:
        yaml.dump(existing_info, f)

    # Save strucutre as base prism input orientaion file (no qualifieers)
    prism_file_name = f"{mp_id}.xyz"

    prism_file_dir = os.path.join(prism_file_dir, mp_id)

    prism_file = os.path.join(prism_file_dir, prism_file_name)

    prismatic_str = pymatgen.io.prismatic.Prismatic(structure).to_str()

    os.makedirs(prism_file_dir, exist_ok=True)

    print(prism_file)

    with open(prism_file, "w") as f:
        f.write(prismatic_str)

    prism_files = [prism_file]
    rotated_files = []

    # Apply rotations
    if rotate:
        if crystal_system == "Cubic":
            rotated_files = crystal_rotation.rotate_cubic_crystal(prism_file)
        else:
            rotated_files = crystal_rotation.rotate_crystal(prism_file)

    prism_files += rotated_files
    if preview_cells:
        for f in prism_files:
            bname = os.path.basename(f)
            fprefix = os.path.splitext(bname)[0]
            zone = fprefix.split("_")[-1]
            title = "".join(zone)
            structure = ase.io.read(f, format="prismatic")
            abtem.show_atoms(structure, plane="xy", title=title)

            plt_name = os.path.join(prism_file_dir, f"{fprefix}.png")
            plt.savefig(plt_name, bbox_inches="tight")
            plt.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="mp_to_prismatic",
        description="Retrieve material from Materiasl Project, apply rotations, and save structure as PRISM input file",
    )

    parser.add_argument(
        "mp_id", help="Materials project ID or file containing Material projects ID"
    )

    parser.add_argument(
        "--rotate",
        action="store_true",
        help="Rotate material along axis of symmetry and save symmetry files",
    )

    parser.add_argument(
        "--preview-cells",
        action="store_true",
        help="Create image of cells",
    )

    parser.add_argument(
        "-d",
        "--prism_file_dir",
        default=DEFAULT_DIR,
        help="Directory to store generated prism input file. Default"
        " is prism_input_files",
    )

    args = parser.parse_args()

    mp_id = args.mp_id
    prism_file_dir = args.prism_file_dir
    rotate = args.rotate
    preview_cells = args.preview_cells

    mp_list = []
    if os.path.isfile(mp_id):
        with open(mp_id, "r") as f:
            flines = f.readlines()

        mp_list = [x.strip() for x in flines]

    else:
        mp_list = [mp_id]

    for mp_id in mp_list:
        mp_to_prismatic(
            mp_id,
            prism_file_dir=prism_file_dir,
            rotate=rotate,
            preview_cells=preview_cells,
        )
