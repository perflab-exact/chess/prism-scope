# PRISM input parameters
See the PRISM documentation on [Input Parameters](https://prism-em.com/docs-params/) for a description of each parameter

## Files

**base_params.txt** = Base PRISM parameters to use for simualtions