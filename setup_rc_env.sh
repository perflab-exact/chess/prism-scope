#!/bin/bash



# Print usage

script_name=$0

usage()
{
    cat <<EOM

DESCRIPTION

  Build script used to create and setup conda environment on
  research computing

SYNOPSIS

  $script_name [--prefix=path] [--destdir=path] [--conf] [--package] [--env-update]

OPTIONS

  --env-update    update conda dependencies listed in the
                  environment.yml file.

EOM
}

# Parse command line

for i in "$@"
do
    case $i in
        --env-update)     env_update=1
                          ;;
        -h | --help)      usage
                          exit 0
                          ;;
        *)                usage
                          exit 1
                          ;;
    esac
done


# Functions to echo and run commands

run() {
    echo "> $1"
    $1 || exit 1
}

# load conda module
run "module load python/miniconda23.3.1"

# Activate Conda Environment
run "source /share/apps/python/miniconda23.3.1/etc/profile.d/conda.sh"

# Create or update environment
if [ -e "environment.yml" ]; then

    # echo "environment.yml file found"
    conda_env=$(head -n 1 environment.yml | cut -f2 -d ' ')

    echo "Conda env is: $conda_env"

    # Check if you are already in the environment
    if [[ $PATH != *$conda_env* ]]; then

        # Check if environment already exists
        conda activate $conda_env
        if [ $? -ne 0 ]; then
            # Create the environment and activate
            echo "Conda env '$conda_env' doesn't exist, creating."
            run "conda env create --solver=libmamba"
            run "conda activate $conda_env"
        fi
    fi
# Require environment file
else
    echo "Could not find environment.yml file, required for running"
    exit 1
fi

if [ -z $CONDA_PREFIX ]; then
    echo "Conda activation did not work, exiting"
    exit 1
fi

if [ $env_update ]; then
    run "conda env update --solver=libmamba"
fi

