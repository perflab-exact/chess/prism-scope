#!/bin/bash
#SBATCH -A __PRISM_SBATCH_ALLOCATION__

#SBATCH -N __PRISM_SBATCH_NODES__
#SBATCH --ntasks-per-node=1

# Run this script N times, passing values 0-N into environment variable SLURM_ARRAY_TASK_ID
# Set end of array to number of max combos
#SBATCH --array=0-__PRISM_NTASKS__%__PRISM_SBATCH_MAX_NTASKS__

#SBATCH -p __PRISM_SBATCH_PARTITION__
#SBATCH --gres=gpu:__PRISM_SBATCH_NUM_GPUS__ # There are 2 GPU's per node
#SBATCH -t __PRISM_SBATCH_TIME__

#SBATCH --job-name __PRISM_SBATCH_JOB_NAME__
#SBATCH --output=%x.%A-%04a.sbatch.log

# Send out mail
#SBATCH --mail-type=ALL
#SBATCH --mail-user=erol.cromwell@pnnl.gov

# INfo about marianas/deception: https://confluence.pnnl.gov/confluence/display/RCWIKI/Marianas

# Functions to echo and run commands

run() {
    echo "> $1"
    $1 || exit 1
}

error_msg="missing expected environment variable."
jobid=${SLURM_ARRAY_JOB_ID:?$error_msg}
array_index=${SLURM_ARRAY_TASK_ID:?$error_msg}

# Path to PRISM input file
prism_input_file=__PRISM_INPUT_FILE__                

# Path to PRISM parameter file
param_file=__PRISM_PARAM_FILE__      

# Path to PRISM combination file
param_combo_file=__PRISM_PARAM_COMBO_FILE__

# Output directory to store PRISM *.h5 file
output_dir=__PRISM_OUTPUT_DIR__

# Log file for relevant information regarding the output file
log_info_file=__PRISM_LOG_INFO_FILE__

# Combination number to run
combo_number=$array_index

# Scratch directory
printf -v scratch_dir  "prism.%d-%04d" \
         $jobid $array_index

# Load modules
run "module purge"
run "module load cuda/11.8"
run "module load python/miniconda23.3.1"


#Is extremely useful to record the modules you have loaded, your limit settings, 
#your current environment variables and the dynamically load libraries that your executable 
#is linked against in your job output file.
echo
echo "loaded modules"
echo
module list >& _modules.lis_
cat _modules.lis_
/bin/rm -f _modules.lis_

echo
echo "Environment Variables"
echo
printenv

echo "HOSTNAME:           " $(hostname)
echo "SLURM_JOB_NAME:      ${SLURM_JOB_NAME}"
echo "SLURM_JOB_ID:        ${SLURM_JOB_ID}"
echo "SLURM_ARRAY_JOB_ID:  ${SLURM_ARRAY_JOB_ID}"
echo "SLURM_ARRAY_TASK_ID: ${SLURM_ARRAY_TASK_ID}"
echo "Scratch Dir:         ${scratch_dir}"

# show the GPU indices on which we will train
echo $CUDA_VISIBLE_DEVICES
echo ""


#Now you can put in your parallel launch command.
#For each different parallel executable you launch we recommend
#adding a corresponding ldd command to verify that the environment
#that is loaded corresponds to the environment the executable was built in.

# This should only be used if using the shared partition
#CUDA_VISIBLE_DEVICES=$CUDA_VISIBLE_DEVICES 
prism_command=__PRISM_COMMAND__

source /share/apps/python/miniconda23.3.1/etc/profile.d/conda.sh
conda activate prism

python_exe="$CONDA_PREFIX/bin/python"

if [ "$param_combo_file" = "None" ]; then
    command="$python_exe $prism_command $prism_input_file $param_file -d $output_dir --log_file=$log_info_file"
else
    command="$python_exe $prism_command $prism_input_file $param_file -d $output_dir --param_combo_file=$param_combo_file --param_combo_number=$combo_number --log_file=$log_info_file"
fi


# cd into tmp working directory (each run creates a scratch_params.txt file)
mkdir -p $scratch_dir
cd $scratch_dir

# Adding sleep to avoid file clobbering when/if reading combo file
sleep $(($combo_number*2+5))

echo "Start Time: " $(date +"%F %T")
echo "Running:    " $command
printf -v start_time "%.2f" $(date +"%s.%N")


$command
exit_code=$?

printf -v end_time "%.2f" $(date +"%s.%N")
echo "End Time:   " $(date +"%F %T")

run_time=$(echo "$end_time - $start_time" | bc)

if (( $(echo "$run_time > 60" | bc) )); then
    run_time=$(echo "scale=2; $run_time / 60" | bc);
    echo "Run Time:    $run_time minutes"
else
    echo "Run Time:    $run_time seconds"
fi

if [ $exit_code -eq 0 ]; then
    echo "Exit Code:   $exit_code = Success"
else
    echo "Exit Code:   $exit_code = Failure"
fi

echo "Command: $command"

exit $exit_code


