#!/usr/bin/env python
# coding: utf-8

import argparse
import glob
import os

import pandas as pd

DIRNAME = os.path.dirname(__file__)
DEFAULT_CSV_DIR = os.path.abspath(os.path.join(DIRNAME, "..", "prism_parameter_files"))
DEFAULT_INPUT_DIR = os.path.abspath(os.path.join(DIRNAME, "..", "prism_input_files"))
DEFAULT_PARAM_FILE = os.path.abspath(
    os.path.join(DIRNAME, "..", "p)rism_parameter_files", "base_params.txt")
)
DEFAULT_OUTPUT_DIR = os.path.abspath(os.path.join(DIRNAME, "..", "prism_output_files"))

SBATCH_CONFIG_TEMPLATE = os.path.abspath(
    os.path.join(
        DIRNAME, "..", "slurm_scripts", "conf", "prism-sbatch.config.template.yml"
    )
)


def generate_sbatch_config_file(
    config_fname,
    run_dir,
    prism_input_file,
    prism_param_file,
    prism_param_combo_file,
    prism_output_dir,
    prism_sbatch_email,
    prism_sbatch_job_name,
):
    sbatch_config = None
    with open(SBATCH_CONFIG_TEMPLATE, "r") as f:
        sbatch_config = f.read()

    replace_dict = {
        "__PRISM_INPUT_FILE__": os.path.abspath(prism_input_file),
        "__PRISM_PARAM_FILE__": os.path.abspath(prism_param_file),
        "__PRISM_PARAM_COMBO_FILE__": os.path.abspath(prism_param_combo_file),
        "__PRISM_OUTPUT_DIR__": os.path.abspath(prism_output_dir),
        "__PRISM_SBATCH_EMAIL__": prism_sbatch_email,
        "__PRISM_SBATCH_JOB_NAME__": prism_sbatch_job_name,
    }

    for field in replace_dict:
        value = replace_dict[field]
        sbatch_config = sbatch_config.replace(field, value)

    config_path = os.path.join(run_dir, config_fname)
    with open(config_path, "w") as f:
        f.write(sbatch_config)

    print(f"Created file {config_path}")


def setup_slurm_run_dir(
    mp_id,
    slurm_run_dir,
    prism_sbatch_email,
    top_output_dir=DEFAULT_OUTPUT_DIR,
    param_file_path=DEFAULT_PARAM_FILE,
    input_file_dir=DEFAULT_INPUT_DIR,
    param_combo_file_path=None,
):

    # Grab all input files
    prism_input_files = glob.glob(f"{input_file_dir}/{mp_id}*xyz")
    prism_input_files.sort()

    # Create the run_idr
    run_dir = os.path.join(slurm_run_dir, mp_id)
    os.makedirs(run_dir, exist_ok=True)

    get_param_file = param_combo_file_path is None

    # create a confif file per input_file
    prism_output_dir = os.path.join(top_output_dir, mp_id)
    count = 0
    for prism_input_file in prism_input_files:
        bname = os.path.basename(prism_input_file)
        fname = os.path.splitext(bname)[0]

        prism_sbatch_job_name = f"prism-{fname}"
        config_fname = f"{fname}.config.yml"

        # Grab matching parameter file
        if get_param_file:
            param_combo_file = f"{fname}_tile_params.csv"
            param_combo_file_path = os.path.join(
                DEFAULT_CSV_DIR, mp_id, param_combo_file
            )

        generate_sbatch_config_file(
            config_fname,
            run_dir,
            prism_input_file,
            param_file_path,
            param_combo_file_path,
            prism_output_dir,
            prism_sbatch_email,
            prism_sbatch_job_name,
        )
        count += 1


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="setup_slurm_run_dir",
        description="Setup slurm directory and config file to run PRISM via SBATCH",
    )

    parser.add_argument("mp_id", help="Material ID")

    parser.add_argument("slurm_run_dir", help="Directory to create slurm run directory")

    parser.add_argument("email", help="Email for slurm config file")

    parser.add_argument(
        "--top_output_dir",
        default=DEFAULT_OUTPUT_DIR,
        help="Top level directory where output h5 files will be stored",
    )

    parser.add_argument(
        "--param_file_path",
        default=DEFAULT_PARAM_FILE,
        help=" Base PRISM parameter file",
    )

    parser.add_argument(
        "--input_file_dir",
        default=DEFAULT_INPUT_DIR,
        help="Directory containing inputs files",
    )

    parser.add_argument(
        "--param_combo_file_path", default=None, help="Path to param combo files"
    )

    args = parser.parse_args()
    mp_id = args.mp_id
    slurm_run_dir = args.slurm_run_dir
    prism_sbatch_email = args.email
    top_output_dir = args.top_output_dir
    param_file_path = args.param_file_path
    input_file_dir = args.input_file_dir
    param_combo_file_path = args.param_combo_file_path

    setup_slurm_run_dir(
        mp_id,
        slurm_run_dir,
        prism_sbatch_email,
        top_output_dir=top_output_dir,
        param_file_path=param_file_path,
        input_file_dir=input_file_dir,
        param_combo_file_path=None,
    )
