#!/usr/bin/env python

import argparse
import os
import subprocess

import pandas as pd
import yaml

CONF_DIR = os.path.join(os.path.dirname(__file__), "conf")

# Dictionary of parameters and default values
# from config file
CONFIG_DEFAULTS = {
    "PRISM_SBATCH_ALLOCATION": "CHESS",
    "PRISM_SBATCH_NODES": "1",
    "PRISM_SBATCH_MAX_NTASKS": "4",
    "PRISM_SBATCH_PARTITION": "all",
    "PRISM_SBATCH_NUM_GPUS": "2",
    "PRISM_SBATCH_TIME": "72:00:00",
    "PRISM_SBATCH_JOB_NAME": None,
    "PRISM_SBATCH_EMAIL": None,
    "PRISM_COMMAND": "$HOME/gitlab.pnnl.gov/chess-materials/research/prism/prism/run_prism.py",
    "PRISM_INPUT_FILE": None,
    "PRISM_PARAM_FILE": None,
    "PRISM_PARAM_COMBO_FILE": "None",
    "PRISM_OUTPUT_DIR": None,
    "PRISM_LOG_INFO_FILE": "prism_h5_file_info.txt",
}

# Valid slurm partitions on Deceptions
VALID_PARTITIONS = ["dl", "dlt", "fat", "shared", "dv", "shared_fat", "all"]


def main(config_file: str, no_submit: bool = False):
    template_file = os.path.join(CONF_DIR, "prism-sbatch.template")

    # Read template file
    file_contents = ""
    with open(template_file, "r") as f:
        file_contents = f.read()

    # Read config file
    with open(config_file, "r") as f:
        config = yaml.safe_load(f)

    # Add default configurations, if none were provided
    for key in CONFIG_DEFAULTS:
        if key not in config:
            config[key] = CONFIG_DEFAULTS[key]

    job_name = config["PRISM_SBATCH_JOB_NAME"]
    if job_name is None:
        job_name = os.path.basename(config["PRISM_COMMAND"])
        config["PRISM_SBATCH_JOB_NAME"] = job_name

    # Check missing requirements
    missing = [key for key in config if config[key] is None]

    if len(missing) > 0:
        print(missing)
        missing_str = ", ".join(missing)
        errmsg = f"Missing required argument(s): {missing_str}\n"
        print(errmsg)
        return

    # Check valid slurm partition
    partition = config["PRISM_SBATCH_PARTITION"]
    if partition not in VALID_PARTITIONS:
        string = f"Error: {partition} is not a valid slurm parition on Deception"
        print(string)
        return

    # PRISM_NTASKS

    param_combo_file = config["PRISM_PARAM_COMBO_FILE"]

    if param_combo_file == "None":
        prism_ntasks = 1
    else:
        combo_df = pd.read_csv(param_combo_file, index_col="combo_id")
        combo_ids = list(combo_df.index)
        ncombos = len(combo_ids)
        prism_ntasks = ncombos - 1

    config["PRISM_NTASKS"] = f"{prism_ntasks}"

    # Replace contents
    for key in config:
        value = config[key]
        file_contents = file_contents.replace(f"__{key}__", value)

    # Write sbatch file
    sbatch_file = f"{job_name}.sbatch"

    with open(sbatch_file, "w") as f:
        f.write(file_contents)

    if no_submit:
        return

    # Run sbatch script
    subprocess.call(["sbatch", sbatch_file])


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="prism-sbatch",
        description="Run multiple instances of a PRISM simulation in parallel using Slurm",
    )

    parser.add_argument(
        "config_file", default=None, help="YAML file with slurm and prism configurations"
    )

    parser.add_argument(
        "--no-submit",
        default=False,
        action="store_true",
        help="Only create the sbatch script, do not submit it.",
    )

    args = parser.parse_args()
    config_file = args.config_file
    no_submit = args.no_submit

    main(config_file, no_submit=no_submit)
