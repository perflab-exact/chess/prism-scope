#!/bin/bash
# Activate environment and run VAP
module load python/miniconda23.3.1
source /share/apps/python/miniconda23.3.1/etc/profile.d/conda.sh
conda activate prism
python -c 'import pyprismatic'
#python /people/crom273/prismatic-chess/prism/run_prism.py $@
