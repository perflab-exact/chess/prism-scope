#!/bin/bash
#SBATCH -A CHESS

#SBATCH -N 1
#SBATCH --ntasks-per-node=1

# Run this script N times, passing values 0-N into environment variable SLURM_ARRAY_TASK_ID
# Set end of array to number of max combos
#SBATCH --array=0-15%4

#SBATCH -p all
#SBATCH --gres=gpu:2 # There are 2 GPU's per node (
#SBATCH -t 72:00:00

#SBATCH -J run_prism
# #SBATCH -o run_prism_%a_out.txt
# #SBATCH -e run_prism_%a_err.txt

#SBATCH --output=%x.%A-%04a.sbatch.log

# Send out mail
#SBATCH --mail-type=ALL
#SBATCH --mail-user=erol.cromwell@pnnl.gov

# https://confluence.pnnl.gov/confluence/display/IC/Marianas

# Functions to echo and run commands

run() {
    echo "> $1"
    $1 || exit 1
}

error_msg="missing expected environment variable."
jobid=${SLURM_ARRAY_JOB_ID:?$error_msg}
array_index=${SLURM_ARRAY_TASK_ID:?$error_msg}

# Path to PRISM input file
fpath="$HOME/prismatic-chess/prism_input_files/mp-5229_1_0_0_angle_0.xyz"                 

# Path to PRISM parameter file
param_file="$HOME/prismatic-chess/prism_parameter_files/base_params.txt"       

# Path to PRISM combination file
param_combo_file=""

# Output directory to store PRISM *.h5 file
OUTPUT_DIR="/qfs/projects/chess/crom273/test_output"

# Combination number to run
combo_number=$array_index

# Load modules
run "module purge"
run "module load cuda/11.8"
run "module load python/miniconda23.3.1"


#Is extremely useful to record the modules you have loaded, your limit settings, 
#your current environment variables and the dynamically load libraries that your executable 
#is linked against in your job output file.
echo
echo "loaded modules"
echo
module list >& _modules.lis_
cat _modules.lis_
/bin/rm -f _modules.lis_

echo
echo "Environment Variables"
echo
printenv

echo "HOSTNAME:           " $(hostname)
echo "SLURM_JOB_NAME:      ${SLURM_JOB_NAME}"
echo "SLURM_JOB_ID:        ${SLURM_JOB_ID}"
echo "SLURM_ARRAY_JOB_ID:  ${SLURM_ARRAY_JOB_ID}"
echo "SLURM_ARRAY_TASK_ID: ${SLURM_ARRAY_TASK_ID}"
# show the GPU indices on which we will train
echo $CUDA_VISIBLE_DEVICES
echo ""


#Now you can put in your parallel launch command.
#For each different parallel executable you launch we recommend
#adding a corresponding ldd command to verify that the environment
#that is loaded corresponds to the environment the executable was built in.

# This should only be used if using the shared partition
#CUDA_VISIBLE_DEVICES=$CUDA_VISIBLE_DEVICES 
prism_command="$HOME/prismatic-chess/prism/run_prism.py"

#source /share/apps/python/miniconda23.3.1/etc/profile.d/conda.sh
#conda activate prism

command="python $prism_command $fpath $param_file -d $OUTPUT_DIR --param_combo_file=$param_combo_file --param_combo_number=$combo_number"


echo "Start Time: " $(date +"%F %T")
echo "Running:    " $command
printf -v start_time "%.2f" $(date +"%s.%N")


#$command
exit_code=$?

printf -v end_time "%.2f" $(date +"%s.%N")
echo "End Time:   " $(date +"%F %T")

run_time=$(echo "$end_time - $start_time" | bc)

if (( $(echo "$run_time > 60" | bc) )); then
    run_time=$(echo "scale=2; $run_time / 60" | bc);
    echo "Run Time:    $run_time minutes"
else
    echo "Run Time:    $run_time seconds"
fi

if [ $exit_code -eq 0 ]; then
    echo "Exit Code:   $exit_code = Success"
else
    echo "Exit Code:   $exit_code = Failure"
fi

echo "Command: $command"

exit $exit_code


