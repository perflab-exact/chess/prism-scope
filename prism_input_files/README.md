## Input info
Use the `materials_map.yml` file in the top level of the repo to find the chemeical formula of the material id

## PRISM input files (*.xyz)
See the PRISM documentation on [Input File Formats](https://prism-em.com/docs-inputs/) for a description on the file format
