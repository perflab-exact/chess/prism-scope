# PRISMATIC

Tools for generating PRISM input files and running [PRISM](https://prism-em.com/) via pyprismatic.

- [PRISMATIC](#prismatic)
  - [Setup Conda Environment](#setup-conda-environment)
  - [Create and Retrieve Materials Project API Key](#create-and-retrieve-materials-project-api-key)
  - [Create PRISM input files](#create-prism-input-files)
  - [Running PRISM software via pyprismatic](#running-prism-software-via-pyprismatic)
  - [Running multiple PRISM simulations](#running-multiple-prism-simulations)
  - [Generate Custom Parameter Combinations](#generate-custom-parameter-combinations)
  - [Directory Structure](#directory-structure)
  - [Running on research computing](#running-on-research-computing)
    - [Setup conda environment on PNNL Research Computing system](#setup-conda-environment-on-pnnl-research-computing-system)
    - [Use `slurm_scripts/prism-sbatch.py` to run multiple PRISM simulations in parallel using Slurm.](#use-slurm_scriptsprism-sbatchpy-to-run-multiple-prism-simulations-in-parallel-using-slurm)
  - [Example Running PRISM on SrTiO3](#example-running-prism-on-srtio3)
    - [1. Download base material structure and create appropriate rotations](#1-download-base-material-structure-and-create-appropriate-rotations)
    - [2. Create the parameter combination files](#2-create-the-parameter-combination-files)
    - [3a. Run PRISM (via pyprismatic) on SrTiO3 rotations sequentially.](#3a-run-prism-via-pyprismatic-on-srtio3-rotations-sequentially)
    - [3b. Alternatively, run PRISM on SrTiO3 rotations in parallel via slurm](#3b-alternatively-run-prism-on-srtio3-rotations-in-parallel-via-slurm)
    - [4. Generate 2D images](#4-generate-2d-images)



## Setup Conda Environment

Download Conda and use the libmamba solver. This will speed up building the conda environment.

```shell
conda install -n base conda-libmamba-solver
conda config --set solver libmamba
```

```shell
conda env create -f environment.yml --solver=libmamba
conda activate prism
```

Use the `prism` conda environment when running all scripts

## Create and Retrieve Materials Project API Key
1. Go to [Materials Project](https://materialsproject.org/) site and create an account
2. Go to your [API page](materialsproject.org/api) and scroll down until you see the box that
   says "Your API Key". Your Materials Project API key is listed under
3. Create the file `prism/materials_project_api.yaml` with the following format
    ```
    api_key: "[API_KEY]"
    ```

    where `API_KEY` is your api key

    ```shell
    chmod 600 prism/materials_project_api.yaml
    ```


## Create PRISM input files
To genereate a PRISM input file using a Material Projets ID

```shell
./prism/mp_to_prismatic.py mat_id [--rotate] [--preview-cells] [-d PRISM_FILE_DIR] 
```
where:
- mp_id = Material ID from [Materials Project](https://materialsproject.org/
- rotate =  Rotate material along axis of symmetry and save symmetry files
- preview-cells = Create and save image of cells
- PRISM_FILE_DIR = Directory to store generated prism input file (and images)

Use the `--rotate` argument with `prism/mp_to_prismatic.py` to generate PRISM input files with the desired rotations. The rotated PRISM fiels have the following notation:

```
[mat_id]_[hkl].xyz
```

where:
- `[mat_id]` is the Materials project ID
- `[hkl]` is the Miller indices vector the structure has been rotated to. This will (ideally) be a symmetric orientation of the structure

For example, runnning:
```shell
./prism/mp_to_prismatic.py mp-5229 --rotate
```

Generates the following files under `prism_input_files/mp-5229`
- `mp-5229.xyz`
- `mp-5229_100.xyz`
- `mp-5229_110.xyz`
- `mp-5229_111.xyz`

To generate input files for several materials, first create a text file (`materials.txt`) and 
add each Material Project ID on individual lines:
```txt
mp-4651
mp-551830
```

then run:
```shell
./prism/mp_to_prismatic.py materials.txt
```

## Running PRISM software via pyprismatic

```shell
./prism/run_prism.py fpath param_file [-d OUTPUT_DIR] [-n OUTPUT_NAME] [--param_combo_file PARAM_COMBO_FILE] [--param_combo_number PARAM_COMBO_NUMBER]  [--log_file LOG_FILE]
```

where:
- `fpath` = Path to PRISM input file
- `param_file` = Path to PRISM parameter file
- `OUTPUT_DIR` =  Output directory to store PRISM *.h5 file
- `OUTPUT_NAME` =  Name of output .h5 file. Default is Same name as PRSIM input file with the *.xyz extension replaced with *.h5
- `PARAM_COMBO_FILE` (Optional) = Parameter combination file to use
- `PARAM_COMBO_NUMBER` (Optional) = Parameter combination number from PARAM_COMBO_FILE
- `LOG_FILE` (Optional)  File to log additional information


## Running multiple PRISM simulations

```shell
./prism/run_mulitple_prism.py input_file_list param_file [-d OUTPUT_DIR] [--param_combo_file PARAM_COMBO_FILE] 
```

where:
  `input_file_list` = Path to text file or PRISM input file to run on of all prism files to run on
  `param_file` = Path to PRISM parameter file
  `OUTPUT_DIR` = Output directory to store PRISM *.h5 file
  `param_combo_file` = Parameter combination file to use

When running `./prism/run_mulitple_prism.py`, it will run PRISM simulations for all parameter combinations in `param_combo_file`

##  Generate Custom Parameter Combinations

You can create your own custum parametr combincation file by using the `prism_generate_param_combos.py` script

```shell
./prism/generate_param_combos.py [-s SAVE_FILE] [-f PARAM_COMBO_FILE]
```
- `SAVE_FILE`: CSV file to save parameter combinations. Default is `parameter_files/parameter_combo.csv`
- `PARAM_COMBO_FILE`: YAML file containing desired parameter variations.

The YAML file `templates/parameter_examples.yml` shows an example. If you run

```shell
./prism/generate_param_combos.py -f templates/parameter_examples.yml -s templates/parameter_examples.csv
```

you will generate `templates/parameter_examples.csv` which will look like:

```csv
combo_id,numFP,tileZ
0,8,26
1,8,63
2,8,120
3,8,250
4,16,26
5,16,63
6,16,120
7,16,250
```

Then, you can run on a spepcific parameter combination wthi the following

```shell
prism/run_prism.py prism_input_files/mp-5229/mp-5229_100.xyz templates/prism_input_params_example.txt --param_combo_file=templates/parameter_examples.csv --param_combo_number=5
```

## Directory Structure
- `materials_map.yml`: YAML file containing mapping of Matierals Project ID to material information. Format per material is:
Example:
    ```yaml
    mat_id (Material Projects ID of material):
        crystal_sytem: Type of crystal system
        pretty_formula: Chemical formula
        symmetry_symbol: Symmetry symbol
    ```

    Example:
    ```yaml
    mp-5229:
        crystal_sytem: Cubic
        pretty_formula: SrTiO3
        symmetry_symbol: Pm-3m
    ```
- `prism_input_files`: PRISM input files (*.xyz format). Files have the following format:
    `[mat_id]_[HKL].xyz`, where
    - `mat_id` = Material Project ID of material (corresponds to ID in `materials_map.yml`)
    - `HKL` = The Miller indices vector the structure has been rotated to. This will (ideally) be a symmetric orientation of the structure

    Example: `mp-5229_110.xyz` is the mp-5229 material (SrTiO3) rotated to the [110] view.

- `prism_parameter_files`: PRISM input paramter files
- `prism_output_files`: PRISM output h5 files, Each file has the following format: `[mat_id]_[HKL]_[t]nm.h5`, where each component is identical to the input file used. Here
    `[t]` is the thickness fo the outpifle in nanometers (nm). Files may also have the fromat `[mat_id]_[hkl]_[t]nm_combo_[c].h5`, where C is the combinatinon index based the parameter
    comination file used.


## Running on research computing

###  Setup conda environment on PNNL Research Computing system
```shell
module load python/miniconda23.3.1
source /share/apps/python/miniconda23.3.1/etc/profile.d/conda.sh
conda env create --solver=libmamba
```

### Use `slurm_scripts/prism-sbatch.py` to run multiple PRISM simulations in parallel using Slurm.

The `prism-sbatch.py` script runs multiple instances of PRISM simulations in parallel
using Slurm. It must be run on a Slurm head node, and will generate and
submit the sbatch script.

The config_file can be created by copying the `prism-sbatch.config.README.yml` file
and editing it for your simulation. The name of the config file does not matter,
but it is good practice to include the material ID. Alternatively, you
can use the `prism-sbatch.config.example.yml` file if you do not need the comments
provided in the README version. These files can be found here:

```
slurm_scripts/conf
```

The sbatch script submitted by prism-sbatch uses a Slurm Job Array to schedule
the individual instances of the PRISM simulations

The sbatch script and log files will be created in the directory where the
`prism-sbatch.py` script was run. These use the following naming conventions:
```
<job_name>.sbatch
<job_name>.<array_jobid>-<array_index>.sbatch.log
```

Any information printed to stdout or stderr by the PRISM simulation will be
captured in the sbatch log files. Error messages from the sbatch jobs
themselves will also be written to these logs.


## Example Running PRISM on SrTiO3
For this example, we will use Strontium Titanate (SrTIO3, mp-5229) as the material. We will use the `prism_paramter_files/base_params.txt` file as our PRISM parameter file


### 1. Download base material structure and create appropriate rotations

```shell
./prism/mp_to_prismatic.py mp-5229 --rotate --preview-cells
```

This will create the following files:

```
prism_input_files/mp-5229/mp-5229.xyz
prism_input_files/mp-5229/mp-5229.png
prism_input_files/mp-5229/mp-5229_100.png
prism_input_files/mp-5229/mp-5229_100.xyz
prism_input_files/mp-5229/mp-5229_110.png
prism_input_files/mp-5229/mp-5229_110.xyz
prism_input_files/mp-5229/mp-5229_111.png
prism_input_files/mp-5229/mp-5229_111.xyz
```

The `*.png` files will show the unit cell of the material. Below is what you should see for `prism_input_files/mp-5229/mp-5229_100.png`:

![Example of SrTiO3](./images/mp-5229_100.png)

### 2. Create the parameter combination files

Run the `./prism/calc_material_thickness.py` script to create the necessary parameter combinations to create PRISM simulations of a 4x4nm material at 10, 25, 50, and 100nm thicknesses

```shell
./prism/calc_material_thickness.py prism_input_files/mp-5229/mp-5229_100.xyz
./prism/calc_material_thickness.py prism_input_files/mp-5229/mp-5229_110.xyz
./prism/calc_material_thickness.py prism_input_files/mp-5229/mp-5229_111.xyz
```

This will create the following files:
```
prism_parameter_files/mp-5229/mp-5229_100_tile_params.csv
prism_parameter_files/mp-5229/mp-5229_110_tile_params.csv
prism_parameter_files/mp-5229/mp-5229_111_tile_params.csv
```

### 3a. Run PRISM (via pyprismatic) on SrTiO3 rotations sequentially.

You can run PRISM on each rotation and paramter combinations by:

```shell
./prism/run_multiple_prism.py prism_input_files/mp-5229/mp-5229_100.xyz prism_paramter_files/base_params.txt --param_combo_file=prism_parameter_files/mp-5229/mp-5229_100_tile_params.csv

./prism/run_multiple_prism.py prism_input_files/mp-5229/mp-5229_110.xyz prism_paramter_files/base_params.txt --param_combo_file=prism_parameter_files/mp-5229/mp-5229_110_tile_params.csv

./prism/run_multiple_prism.py prism_input_files/mp-5229/mp-5229_111.xyz prism_paramter_files/base_params.txt --param_combo_file=prism_parameter_files/mp-5229/mp-5229_111_tile_params.csv

```

This will generate the 10, 25, 50, 100nm output PRISM files for the rotations

```
prism_output_files/mp-5229/mp-5229_100_100nm_combo_3.h5
prism_output_files/mp-5229/mp-5229_100_10nm_combo_0.h5
prism_output_files/mp-5229/mp-5229_100_25nm_combo_1.h5
prism_output_files/mp-5229/mp-5229_100_50nm_combo_2.h5
prism_output_files/mp-5229/mp-5229_110_100nm_combo_3.h5
prism_output_files/mp-5229/mp-5229_110_10nm_combo_0.h5
prism_output_files/mp-5229/mp-5229_110_25nm_combo_1.h5
prism_output_files/mp-5229/mp-5229_110_50nm_combo_2.h5
prism_output_files/mp-5229/mp-5229_111_100nm_combo_3.h5
prism_output_files/mp-5229/mp-5229_111_10nm_combo_0.h5
prism_output_files/mp-5229/mp-5229_111_25nm_combo_1.h5
prism_output_files/mp-5229/mp-5229_111_50nm_combo_2.h5
```

In the filenames, the `[C]` in `_combo_[C]` corresponds to the `combo_id` column value found in the PRISM paramter file. For example, for `mp-5229_111_50nm_combo_2.h5`, the `combo_2` corresponds to the row with value `2` in column `combo_id` in `prism_parameter_files/mp-5229/mp-5229_111_tile_params.csv`

### 3b. Alternatively, run PRISM on SrTiO3 rotations in parallel via slurm

If you have access to a slurm cluster, you can run the PRISM simulations in parallel using `prism-sbatch.py`. You will need to create config file for each rotation. Below is an example
for the `mp-5229_100.xyz` input (save as `mp-5229_100-config.yml`)

```yaml
#filename: mp-5229_100-config.yml

# Required Variables

# Input PRISM File
PRISM_INPUT_FILE: "[PATH_TO_REPO]/prism_input_files/mp-5229/mp-5229_100.xyz"

# Base PRISM parameter file
PRISM_PARAM_FILE: "[PATH_TO_REPO]/prism_parameter_files/base_params.txt"    

# PRISM parameter combinatino file
PRISM_PARAM_COMBO_FILE:  "[PATH_TO_REPO]/prism_parameter_files/mp-5229/mp-5229_100_tile_params.csv"

# Output directory of PRISM *.h5 files
PRISM_OUTPUT_DIR: "[PATH_TO_REPO]/prism_output_files/mp-5229"

# Email to send slurm job status updates
PRISM_SBATCH_EMAIL: "your.email@domain.ext"

# Optional Variables

PRISM_COMMAND: "[PATH_TO_REPO]/prism/run_prism.py"

# Default: Process name or executable name from PRISM_COMMAND
PRISM_SBATCH_JOB_NAME: "prism-mp-5229-100"

PRISM_SBATCH_ALLOCATION: Your SBATCH Allocation

# Default: 1 Node
#PRISM_SBATCH_NODES: "1"

# Default: Maximum of 4 tasks at a time
#PRISM_SBATCH_MAX_NTASKS: "4"

# Default: The 'all' parition
#PRISM_SBATCH_PARTITION: all

# Default: 2 GPUS
# PRISM_SBATCH_NUM_GPUS: ""2"

# Default: 3 days
# PRISM_SBATCH_TIME": "72:00:00
```

where `[PATH_TO_REPO]` is the directory path of the repository

Then, create similar `mp-5229_110.config.yml` and `mp-5229_111.config.yml` files.

Alternatively, you can use `slurm_scripts/setup_slurm_run_dir.py` to set up the necessary directories and config files:

```shell
slurm_scripts/setup_slurm_run_dir.py --top_output_dir=prism_output_files --param_file_path=prism_parameter_files/base_params.txt --input_file_dir=prism_input_files/mp-5229 --param_combo_file_path=prims_paramter_files/mp-5229 mp-5229 prism_slurm_runs your.email@domain.ext
```

Be sure to replace `your.email@domain.ext` with your email address

This wil create the following directories and files

```
prism_slurm_runs/mp-5229/mp-5229.config.yml
prism_slurm_runs/mp-5229/mp-5229_100.config.yml
prism_slurm_runs/mp-5229/mp-5229_110.config.yml
prism_slurm_runs/mp-5229/mp-5229_111.config.yml
```


Then, submit the slurm jobs:

```shell
./slurm_scripts/prism-sbatch.py prism_slurm_runs/mp-5229/mp-5229_100.config.yml
./slurm_scripts/prism-sbatch.py prism_slurm_runs/mp-5229/mp-5229_110.config.yml
./slurm_scripts/prism-sbatch.py prism_slurm_runs/mp-5229/mp-5229_111.config.yml
```

This will output the same files in 3a in the same location (`prism_output_files/mp-5229`)

### 4. Generate 2D images

We will create the 2D images for all the simulations by using the `prism/print_multi_prism_images.py` script

```shell
prism/print_multi_prism_images.py -o prism_output_files/mp-5229 -d prism_output_images/mp-5229
```
For each output `*h5` file, this will generate the following images under `prism_output_images/mp-5229`
- 4 different looks from different detection angles
    - `*_BF.tiff`: virtual bright field (BF)
    - `*_ABF.tiff`: annular bright field (ABF)
    - `*_ADF.tiff`: annular dark field (ADF)
    - `*_HAADF.tiff`: high angle annular dark field (HAADF)
- A summary image (`*_summary.png`) with all 4 of the above

For example, `mp-5229_111_50nm_combo_2.h5` will have the following images generated:
```
prism_output_images/mp-5229/mp-5229_111_50nm_combo_2_ABF.tiff
prism_output_images/mp-5229/mp-5229_111_50nm_combo_2_ADF.tiff
prism_output_images/mp-5229/mp-5229_111_50nm_combo_2_BF.tiff
prism_output_images/mp-5229/mp-5229_111_50nm_combo_2_HAADF.tiff
prism_output_images/mp-5229/mp-5229_111_50nm_combo_2_summary.png
```